package com.bplus;

import com.bplus.helper.Logger;
import com.bplus.manager.BPlusTree;
import com.bplus.manager.BPlusTreeNext;
import com.bplus.system.IOHandler;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {


    public static void main(String[] args) {

        /*
            INITALIZE
         */

        // data row size
        Integer m = 3;

        // the input and output handler
        IOHandler mIOHandler = new IOHandler();

        // the main b+tree manager
        BPlusTree mBPlusTree = new BPlusTree(m);






        /*
            DOING THE HARD WORK
         */

        // read the input data file
        List<Pair<Integer, Integer>> dataList = mIOHandler.readInput("/Users/claudia/Documents/input.csv");


        BPlusTreeNext aa = new BPlusTreeNext(3);

        // loop through input file and add data to b plus tree
        int cnt = 1;
        for (Pair<Integer, Integer> pair : dataList) {

            // insert new data to b+ tree
            mBPlusTree.insert(pair.getKey(), pair.getValue());


            aa.insert(pair.getKey(), pair.getValue());

            // log action
            System.out.print("insert cnt: " + String.format("%05d", cnt++) + " key:" + pair.getKey() + " val:" + pair.getValue() + "\n");
        }


        aa.find(37);
        aa.find(40);





        // search for key
        Integer searchKey = 41300;


        // attempt to search for key
        mBPlusTree.search(searchKey);



        // log node
//        mBPlusTree.export();
    }
}
