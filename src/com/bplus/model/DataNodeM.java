package com.bplus.model;

import com.bplus.helper.Logger;

import java.util.ArrayList;

public class DataNodeM extends NodeM {

    public DataNodeM(Integer _mDataArraySize) {
        super(_mDataArraySize);
    }

    /**
     * Insert new item(key, data) into this node
     * @param key
     * @param value
     * @return
     */
    public NodeInsertResultM insert(Integer key, Integer value) {

        // get where to insert data
        Integer insertPosition = getInsertPosition(key);

        if ((insertPosition < mItemCount) && (mKeyList.get(insertPosition) == key)) {
            mDataList.set(insertPosition, value);
            return null;
        }

        mKeyList.add(insertPosition, key);
        mDataList.add(insertPosition, value);
        mItemCount++;

        // check if we need to rebalance the tree
        if (mItemCount <= this.mDataArraySize) {
            return null;
        }

        // get the center index count
        Integer centerItemCount = ((mItemCount + 1)/2);

        // create new node
        DataNodeM newNode = new DataNodeM(this.mDataArraySize);
        newNode.setItemCount(mItemCount - centerItemCount);
        newNode.setKeyList(new ArrayList<Integer>(mKeyList.subList(centerItemCount, mItemCount)));
        newNode.setDataList(new ArrayList<Integer>(mDataList.subList(centerItemCount, mItemCount)));

        // update current node values
        setItemCount(centerItemCount);
        setKeyList(new ArrayList<Integer>(mKeyList.subList(0, centerItemCount)));
        setDataList(new ArrayList<Integer>(mDataList.subList(0, centerItemCount)));

        // return new balanced node
        return new NodeInsertResultM(newNode.getKeyList().get(0), this, newNode);
    }

    /**
     * Search for a specific key
     */
    public void search(Integer searchKey) {
        for(int i = 0; i < mKeyList.size(); i++) {
            if (mKeyList.get(i) == searchKey) {
                Logger.console("\n" + mKeyList.get(i) + "," + mDataList.get(i));
            }
        }
    }

    /**
     *  Export the key and data content
     */
    public void export() {

        // new line
        Logger.console("\n");

        // loop throught key and data, export to console
        for(int i = 0; i < mKeyList.size(); i++) {
            Logger.console(mKeyList.get(i) + "["+mDataList.get(i)+"]");
        }

        // call children subnode to export
        for(NodeM subNode : mDataNodeList) {
            subNode.export();
        }
    }
}
