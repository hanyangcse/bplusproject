package com.bplus.model;

import com.bplus.helper.Logger;

import java.util.ArrayList;

public class IndexNodeM extends NodeM {

    public IndexNodeM(Integer _mDataArraySize) {
        super(_mDataArraySize);
    }

    /**
     * Insert new item(key, data) into this node
     * @param key
     * @param value
     * @return
     */
    public NodeInsertResultM insert(Integer key, Integer value) {

        // get where to insert data
        Integer insertPosition = getInsertPosition(key);

        NodeM nodeAtInsertPosition = mDataNodeList.get(insertPosition);
        NodeInsertResultM insertResult = nodeAtInsertPosition.insert(key, value);

        if (insertResult == null) {
            return null;
        }

        Integer newInsertPosition = getInsertPosition(insertResult.getKey());
        mKeyList.add(newInsertPosition, insertResult.getKey());
        mDataNodeList.set(newInsertPosition, insertResult.getLeftNode());
        mDataNodeList.add((newInsertPosition + 1), insertResult.getRightNode());
        mItemCount++;

        // check if we need to rebalance the tree
        if (mItemCount <= mDataArraySize) {
            return null;
        }

        // find the center item count to rebalance tree
        Integer centerItemCount = (mItemCount/2);

        // create new node from rebalancing
        IndexNodeM newNode = new IndexNodeM(mDataArraySize);
        newNode.setItemCount(mItemCount - centerItemCount - 1);
        newNode.setKeyList(new ArrayList<Integer>(mKeyList.subList((centerItemCount + 1), mItemCount)));
        newNode.setDataNodeList(new ArrayList<NodeM>(mDataNodeList.subList((centerItemCount + 1), (mItemCount + 1))));

        // create split node
        NodeInsertResultM newNodeAndPosition = new NodeInsertResultM(mKeyList.get(centerItemCount),this, newNode);

        // update current node values with rebalance tree
        setItemCount(centerItemCount);
        setKeyList(new ArrayList<Integer>(mKeyList.subList(0, centerItemCount)));
        setDataNodeList(new ArrayList<NodeM>(mDataNodeList.subList(0, (centerItemCount + 1))));

        // retrun new rebalance node
        return newNodeAndPosition;
    }

    /**
     * Search for a specific key
     */
    public void search(Integer searchKey) {

        for(int i = 0; i < mKeyList.size(); i++) {
            if (mKeyList.get(i) == searchKey) {
                mDataNodeList.get(i).search(searchKey); return;
            }
        }


        // not found, lets loop through children item's
        for(int now = 0, next = 1; now < mKeyList.size(); now++, next++) {

            // current key
            Integer nowKey = mKeyList.get(now);

            // next key
            Integer nextKey = (next < mKeyList.size()) ? mKeyList.get(next) : null;


            // check if search key is less then current key
            // this is for array[0] position, the first item
            if (searchKey < nowKey) {
                mDataNodeList.get(now).search(searchKey); return;
            }
            // check if search ky is larger than current key, but next key does not exsist
            // this is for the last position of the array
            else if ((nowKey <= searchKey) && (nextKey == null)) {
                mDataNodeList.get(now).search(searchKey); return;
            }

            // check if search key is larger than previous but smaller than or equal to the next
            // this is for the middle objects in the array
            else if ((searchKey < nowKey) && (searchKey <= nextKey)) {
                mDataNodeList.get(nextKey).search(searchKey); return;
            }
        }
    }

    /**
     *  Export the key and data content
     */
    public void export() {

        // new line
        Logger.console("\n IndexNode \n");

        // loop throught key and data, export to console
        for(int i = 0; i < mKeyList.size(); i++) {
            Logger.console(mKeyList.get(i) + "");
        }

        // call children subnode to export
        for(NodeM subNode : mDataNodeList) {
            subNode.export();
        }
    }
}