package com.bplus.model;

import com.bplus.helper.Logger;

import java.util.ArrayList;
import java.util.List;

public class NodeM {

    /**
     * A list that holds the indexing key's
     */
    public List<Integer> mKeyList = new ArrayList<Integer>();

    /**
     * A list that holds the extra data per index
     */
    public List<Integer> mDataList = new ArrayList<Integer>();

    /**
     * A list that holds the sub node list
     */
    public List<NodeM> mDataNodeList = new ArrayList<NodeM>();

    /**
     * max number of child for each data node (m#)
     */
    Integer mDataArraySize = 0;

    /**
     * actual number of child for each data node
     */
    public Integer mItemCount = 0;


    /**
     * Initalize node with max siz
     * @param _mDataArraySize integer
     */
    public NodeM(Integer _mDataArraySize) {
        this.mDataArraySize = _mDataArraySize;
    }

    /**
     * get the position for the next node it be inputed
     * @param key the new key index
     * @return integer position to insert new item
     */
    public int getInsertPosition(Integer key) {
        for (int i = 0; i < mItemCount; i++) {
            Integer item = mKeyList.get(i);
            if (item.compareTo(key) >= 0) {
                return i;
            }
        }
        return 0;
    }

    /**
     * Insert new node into array.
     * This function will be overwritten by subclasses
     * @param key
     * @param value
     * @return
     */
    public NodeInsertResultM insert(Integer key, Integer value) {
        return null;
    }

    /**
     *  Export the key and data content
     */
    public void export() {
        return;
    }

    /**
     * Return a list holding the the index key
     * @return A integer list
     */
    public List<Integer> getKeyList() {
        return mKeyList;
    }

    /**
     * Set the key list
     * @param _mKeyList
     */
    public void setKeyList(List<Integer> _mKeyList) {
        this.mKeyList = _mKeyList;
    }

    /**
     * Return a list holding all the data
     * @return A integer List
     */
    public List<Integer> getDataList() {
        return mDataList;
    }

    /**
     * Set Data list
     * @param _mDataList
     */
    public void setDataList(List<Integer> _mDataList) {
        this.mDataList = _mDataList;
    }

    /**
     * Return a list holding all the data node
     * @return
     */
    public List<NodeM> getDataNodeList() {
        return mDataNodeList;
    }

    /**
     * Set the data list
     * @param _mDataNodeList
     */
    public void setDataNodeList(List<NodeM> _mDataNodeList) {
        this.mDataNodeList = _mDataNodeList;
    }

    /**
     * Set the item counter value
     * @param _mItemCount
     */
    public void setItemCount(Integer _mItemCount) {
        mItemCount = _mItemCount;
    }

    /**
     * Return the item count
     * @return
     */
    public Integer getItemCount() {
        return mItemCount;
    }

    /**
     * Search for a specific key
     */
    public void search(Integer searchKey) {

    }
}