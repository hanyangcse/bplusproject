package com.bplus.model;

public class NodeInsertResultM {

    /**
     * the index's key
     */
    private Integer key;

    /**
     * the left node pointer
     */
    private NodeM leftNode;

    /**
     * the right node pointer
     */
    private NodeM rightNode;


    public NodeInsertResultM(Integer _key, NodeM _leftNode, NodeM _rightNode) {
        this.key = _key;
        this.leftNode = _leftNode;
        this.rightNode = _rightNode;
    }

    /**
     * Create a new node using the key, leftnode, rightnode
     * @param _mDataArraySize size of data node
     * @return new IndexNode object
     */
    public IndexNodeM createNode(Integer _mDataArraySize) {
        IndexNodeM newNode = new IndexNodeM(_mDataArraySize);
        newNode.mItemCount = 1;
        newNode.mKeyList.add(0, key);
        newNode.mDataNodeList.add(0, leftNode);
        newNode.mDataNodeList.add(1, rightNode);
        return newNode;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer _key) {
        key = _key;
    }

    public NodeM getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(NodeM _leftNode) {
        leftNode = _leftNode;
    }

    public NodeM getRightNode() {
        return rightNode;
    }

    public void setRightNode(NodeM _rightNode) {
        rightNode = _rightNode;
    }
}