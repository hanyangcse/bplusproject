package com.bplus.system;

import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * This handler will process the input and output of our data
 */
public class IOHandler {

    /**
     * reads throught the input file
     * @param filename the input file name
     * @return a map holding key and data for the b plus tree
     */
    public List<Pair<Integer, Integer>> readInput(String filename ) {

        // input file object
        File inputFile = new File(filename);

        // map to hold all the data in file
        List<Pair<Integer, Integer>> dataList = new ArrayList<Pair<Integer, Integer>>();

        try{
            // create scanner to scan file
            Scanner inputStream = new Scanner(inputFile);

            // list through rows in file
            while(inputStream.hasNext()) {
                // text line
                String line = inputStream.next();

                // get row array
                String[] row = line.split(",");

                // save data to map
                dataList.add(new Pair<Integer, Integer>(Integer.parseInt(row[0]), Integer.parseInt(row[1])));
            }

            // close file stream
            inputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return dataList;
    }
}
