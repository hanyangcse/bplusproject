package com.bplus.manager;

import com.bplus.helper.Logger;
import com.bplus.model.*;

public class BPlusTree {

    // the main top starting node
    private NodeM rootNode = null;

    // number of child for each data node (m#)
    private Integer mDataArraySize = 0;


    public BPlusTree(Integer _mDataArraySize) {
        mDataArraySize = _mDataArraySize;
        rootNode = new DataNodeM(mDataArraySize);
    }

    /**
     * Search for key or a set of keys
     */
    public void search(Integer searchKey) {

        // start search at root node
        rootNode.search(searchKey);
    }

    /**
     * Add new key & data to the B+ Tree
     */
    public void insert(Integer key, Integer val) {

        // insert val to b+ tree
        NodeInsertResultM result = rootNode.insert(key, val);

        // check if we found a index node
        if (result == null) {
            return;
        }

        // create new node, update the root node
        rootNode = result.createNode(mDataArraySize);
    }

    /**
     * Remove a data item by key from the tree
     */
    public void delete() {

    }

    /**
     * Return main root node
     */
    public NodeM getRootNode() {
        return rootNode;
    }

    /**
     * Export to console
     */
    public void export() {

        // call recursive export
        rootNode.export();
    }
}
